﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using eDziennik.App_SchoolDb;
using eDziennik.App_Code.Grupa_2;

namespace eDziennik
{
    public partial class AuthenticationView : System.Web.UI.Page
    {
        private IAuthentication authentication;

        protected void Page_Load(object sender, EventArgs e)
        {
            authentication = new Authentication();
        }

        protected void Login1_LoggedIn(object sender, EventArgs e)
        {
            Guid userID = authentication.GetPersonIdByLogin(Login1.UserName);

            IList<String> idAndRole = authentication.GetPersonIdAndRoleByUserId(userID).ToList();

            Session["Id"] = Convert.ToInt32(idAndRole[0]);
            Session["RoleName"] = idAndRole[1];

            if (Session["RoleName"].ToString() == "teacher")
            {
                //Response.Redirect("/App_View/Grupa_2/GradesViewTeacher.aspx");
                //główna strona dla nauczyciela
            }
            else if (Session["RoleName"].ToString() == "student")
            {
                Response.Redirect("/App_View/Grupa_2/GradesViewStudent.aspx");
                //głowna strona dla ucznia
            }
            else if (Session["RoleName"].ToString() == "admin")
            {
                //głowna strona dla admina
            }
            else
            {
                Response.Redirect("/App_Error_Page/DefaultError.html");
            }
            
        }
    }
}