﻿using eDziennik.App_Code.Grupa_2;
using eDziennik.App_SchoolDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eDziennik.App_View.Grupa_2
{
    public partial class StudentEventsView : System.Web.UI.Page
    {
        protected IStudentLatestNews service;
        protected IEnumerable<Event> StudentEvents;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["RoleName"] != null && Session["RoleName"] == "student")
            {
                int studentId = (int)Session["id"];
                service = new StudentLatestNews(studentId);

                FillStudentWall();
            }
            else
                Response.Redirect("AuthenticationViewLogin.aspx");
        }

        private void FillStudentWall()
        {
            this.StudentEvents = this.service.StudentNextEvents();
        }
    }
}