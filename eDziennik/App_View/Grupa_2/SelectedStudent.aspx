﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="SelectedStudent.aspx.cs" Inherits="eDziennik.App_View.Grupa_2.SelectedStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
    <a href="GradesViewTeacher.aspx">Powrót</a><br />
    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true" Width="185px" Visible="false" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
        <asp:ListItem Selected="True" Text="Widok ogólny"></asp:ListItem>
        <asp:ListItem Text="Widok szczegółowy"></asp:ListItem>
    </asp:RadioButtonList><br />
    <asp:GridView ID="GridView1" runat="server" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
        <Columns>
            <asp:CommandField CancelText="Anuluj" DeleteText="Usuń" EditText="Edytuj" ShowEditButton="True" UpdateText="Zapisz" />
        </Columns>
    </asp:GridView>
    <asp:Label ID="Label2" runat="server" Text="" Visible="false"></asp:Label><asp:TextBox ID="TextBox1" runat="server" Width="40" Visible="false"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="Wystaw ocenę końcową" OnClick="Button1_Click" Visible="false"/>
</asp:Content>