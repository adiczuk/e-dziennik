﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="GradesViewStudent.aspx.cs" Inherits="eDziennik.App_View.Grupa_2.GradesViewStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Wybierz przedmiot:"></asp:Label><br />
    <asp:DropDownList ID="ddlPrzedmioty" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlPrzedmioty_SelectedIndexChanged"></asp:DropDownList><br />
    <asp:GridView ID="GridView1" runat="server">
    </asp:GridView><br />
    <asp:Label ID="Label2" runat="server" Text="Aktualnie brak ocen z tego przedmiotu" Visible="false"></asp:Label>
</asp:Content>
