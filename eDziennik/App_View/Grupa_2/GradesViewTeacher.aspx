﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="GradesViewTeacher.aspx.cs" Inherits="eDziennik.App_View.Grupa_2.GradesViewTeacher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>Wybierz przedmiot:</td>
            <td><asp:DropDownList ID="ddlPrzedmioty" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlPrzedmioty_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>Wybierz klasę:</td>
            <td><asp:DropDownList ID="ddlKlasy" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlKlasy_SelectedIndexChanged"></asp:DropDownList></td>
            
        </tr>
        <tr>
            <td>Wybierz ucznia:</td>
            <td><asp:DropDownList ID="ddlUczniowie" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlUczniowie_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="Button1" runat="server" Text="Pokaż oceny" Width="" OnClick="Button1_Click" Visible="False" />
            </td>
        </tr>
    </table> 
    </asp:Content>
