﻿<%@ Page Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="StudentLatestNews.aspx.cs" Inherits="eDziennik.App_View.Grupa_2.StudentLatestNewsView" %>

<asp:Content ContentPlaceHolderID="head" ID="content1" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="content2" runat="server">
    <style>


        #wrapper {
            width: 900px;
            overflow: hidden;
        }
        #first {
            width: 500px;
            float:left; 

        }
        #second {
            overflow: hidden; 

       
        }

    </style>
    <div id="wrapper">
        <div id="first">
    <table>
        <th>Twoja ściana</th>
      <% foreach (var message in WallMessages)
         { %>
        <tr><td><%= message %></td></tr>
      <% } %>
    </table></div>
        <div id="second">
            
            <table>
        <th>Nadchodzące wydarzenia</th>
      <% foreach (var eventStudent in StudentNextFiveEvents)
         { %>
        <tr><td><%= eventStudent.EventData %></td><td><%= eventStudent.EventContent %></td></tr>
      <% } %>
               <tr><td><a href="StudentEvents.aspx">Kliknij aby zobaczyć wszystkie wydarzenia</a></td></tr> 
    </table>


        </div>

    </div>


</asp:Content>