﻿<%@ Page Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="StudentEvents.aspx.cs" Inherits="eDziennik.App_View.Grupa_2.StudentEventsView" %>

<asp:Content ContentPlaceHolderID="head" ID="content1" runat="server"></asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="content2" runat="server">

    <table>
        <th>Twoje wydarzenia</th>
      <% foreach (var eventStudent in StudentEvents)
         { %>
        <tr><td><%= eventStudent.EventData %></td><td><%= eventStudent.EventContent %></td></tr>
      <% } %>
    </table>



</asp:Content>