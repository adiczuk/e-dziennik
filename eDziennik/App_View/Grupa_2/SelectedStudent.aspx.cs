﻿using eDziennik.App_Code.Grupa_2;
using eDziennik.App_SchoolDb;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eDziennik.App_View.Grupa_2
{
    public partial class SelectedStudent : System.Web.UI.Page
    {
        IGrades grades;
        String nazwiskoIImie;
        String kalsa;
        String przedmiot;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["RoleName"].ToString() == "teacher")
            {
                grades = new Grades(null, int.Parse(Session["Id"].ToString()));

                try
                {
                    nazwiskoIImie = Request.Cookies["uczen"].Value;
                    kalsa = Request.Cookies["klasa"].Value;
                    przedmiot = Request.Cookies["przedmiot"].Value;
                }
                catch (Exception)
                {
                    Response.Redirect("GradesViewTeacher.aspx");
                }

                Label1.Text = "Wybrany uczeń: " + nazwiskoIImie;

                if (!IsPostBack)
                    WypelnijGV();
                else
                    DodajStopke();
            }
            else
            {
                Response.Redirect("/App_Error_Page/DefaultError.html");
            }
        }

        private void WypelnijGV()
        {
            string imie = nazwiskoIImie.Split(' ')[1];
            string nazwisko = nazwiskoIImie.Split(' ')[0];

            if (!(nazwisko == "Wszyscy" && imie == "uczniowie"))
            {
                GridView1.Columns[0].Visible = true;
                RadioButtonList1.Visible = false;
                GridView1.ShowFooter = true;

                Student student = grades.GetStudentByNameAndSurname(imie, nazwisko);
                IList<Grade> oceny = grades.TeacherStudentsGradesForSubject(student, grades.GetSubjectByName(przedmiot)).ToList();

                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("Ocena", typeof(string)));
                dt.Columns.Add(new DataColumn("Data", typeof(string)));   
                dt.Columns.Add(new DataColumn("Opis", typeof(string)));

                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
                double suma = 0;
                double srednia;
                double ilosc = 0;
                bool czy_koncowa = false;

                foreach (Grade ocena in oceny)
                {
                    dr = dt.NewRow();                    
                    dr[0] = ocena.GradeValue.ToString();
                    dr[1] = ocena.GradeDate.ToShortDateString();
                    dr[2] = ocena.GradeContent;

                    if (ocena.GradeType == GradeType.Final)
                        czy_koncowa = true;
                    
                    dt.Rows.Add(dr);

                    suma += ocena.GradeValue;
                    ilosc++;
                }

                if (ilosc > 0)
                {
                    PokazKontrolki(true);
                    srednia = Math.Round(suma / ilosc, 2);
                    Label2.Text = "Średnia z ocen wynosi " + srednia.ToString() + ", po zaokrągleniu ";
                    double zaokrąglona;
                    try
                    {
                        if (int.Parse(Math.Round(srednia, 1).ToString().Split(',')[1]) < 5)
                            zaokrąglona = Math.Floor(srednia);
                        else
                            zaokrąglona = Math.Ceiling(srednia);
                    }
                    catch (Exception)
                    {
                        zaokrąglona = srednia;
                    }

                    Session["ocena"] = TextBox1.Text;
                    TextBox1.Text = zaokrąglona.ToString();
                }
                else
                {
                    PokazKontrolki(false);
                }

                GridView1.DataSource = dt;
                GridView1.DataBind();
                GridView1.Rows[0].Visible = false;
                if (!czy_koncowa)
                {
                    DodajStopke();
                    PokazKontrolki(true);
                }
                else
                    PokazKontrolki(false);
            }
            else
            {
                GridView1.Columns[0].Visible = false;
                RadioButtonList1.Visible = true;
                GridView1.ShowFooter = false;

                DataTable dt = new DataTable();

                if (RadioButtonList1.SelectedIndex == 0)
                {
                    IList<Student> studenci = grades.TeacherStudentsForClass(grades.GetClassBySymbolAndRank(this.kalsa.Split(' ')[0], this.kalsa.Split(' ')[1])).OrderBy(s => s.StudentLastName).ToList();

                    dt.Columns.Add(new DataColumn("Nazwisko i imię", typeof(string)));
                    foreach (Student student in studenci)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = student.StudentLastName + " " + student.StudentFirstName;
                        int i = 1;
                        foreach (Grade ocena in grades.TeacherStudentsGradesForSubject(student, grades.GetSubjectByName(this.przedmiot)).ToList())
                        {
                            try
                            {
                                dr[i] = ocena.GradeValue.ToString();                                
                            }
                            catch (Exception)
                            {
                                dt.Columns.Add(new DataColumn("Ocena " + i, typeof(string)));
                                dr[i] = ocena.GradeValue.ToString();
                            }
                            if (ocena.GradeType == GradeType.Final)
                                dr[i] += " KOŃCOWA";
                            i++;
                        }
                        dt.Rows.Add(dr);
                    }

                }
                else
                {
                    IList<Student> studenci = grades.TeacherStudentsForClass(grades.GetClassBySymbolAndRank(this.kalsa.Split(' ')[0], this.kalsa.Split(' ')[1])).OrderBy(s => s.StudentLastName).ToList();

                    dt.Columns.Add(new DataColumn("Nazwisko i imię", typeof(string)));
                    dt.Columns.Add(new DataColumn("Ocena", typeof(string)));
                    dt.Columns.Add(new DataColumn("Data", typeof(string)));
                    dt.Columns.Add(new DataColumn("Opis", typeof(string)));
                    foreach (Student student in studenci)
                    {
                        foreach (Grade ocena in grades.TeacherStudentsGradesForSubject(student, grades.GetSubjectByName(this.przedmiot)).ToList())
                        {
                            DataRow dr = dt.NewRow();
                            dr[0] = student.StudentLastName + " " + student.StudentFirstName;
                            dr[1] = ocena.GradeValue.ToString();
                            dr[2] = ocena.GradeDate.ToShortDateString();
                            dr[3] = ocena.GradeContent;                            
                            dt.Rows.Add(dr);
                        }
                    }
                }
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }

        }

        private void PokazKontrolki(bool p)
        {
            Label2.Visible = p;
            TextBox1.Visible = p;
            Button1.Visible = p;
        }

        private void DodajStopke()
        {
            Button dodaj = new Button();
            dodaj.Click += new EventHandler(dodaj_Click);
            dodaj.Text = "Dodaj";
            TextBox ocenaTB = new TextBox();
            ocenaTB.Width = 40;
            Calendar dataC = new Calendar();
            dataC.SelectedDate = DateTime.Today;
            dataC.SelectionChanged += new EventHandler(dataC_SelectionChanged);
            DropDownList typ = new DropDownList();
            typ.Items.Add("Normal");
            typ.Items.Add("Final");
            typ.Items[0].Selected = true;
            TextBox opisTB = new TextBox();

            GridView1.FooterRow.Cells[0].Controls.Add(dodaj);
            GridView1.FooterRow.Cells[1].Controls.Add(ocenaTB);
            GridView1.FooterRow.Cells[2].Controls.Add(dataC);
            GridView1.FooterRow.Cells[3].Controls.Add(opisTB);
        }

        void dataC_SelectionChanged(object sender, EventArgs e)
        {
            Session["data"] = ((Calendar)GridView1.FooterRow.Cells[2].Controls[0]).SelectedDate.ToShortDateString();
        }

        private void dodaj_Click(object sender, EventArgs e)
        {
            double value;
            DateTime data;
            try
            {
                String[] sdata = Session["data"].ToString().Split('-');
                data = new DateTime(int.Parse(sdata[0]), int.Parse(sdata[1]), int.Parse(sdata[2].Split(' ')[0]));
            }
            catch (Exception)
            {
                data = ((Calendar)GridView1.FooterRow.Cells[2].Controls[0]).SelectedDate;
            }           
            string opis = ((TextBox)GridView1.FooterRow.Cells[3].Controls[0]).Text;

            if (this.IsGoodValue(((TextBox)GridView1.FooterRow.Cells[1].Controls[0]).Text, out value))
            {
                Grade ocena = new Grade
                {
                    StudentId = grades.GetStudentByNameAndSurname(this.nazwiskoIImie.Split(' ')[1], this.nazwiskoIImie.Split(' ')[0]).StudentId,
                    GradeValue = value,
                    GradeDate = data,
                    GradeType = GradeType.Normal,
                    GradeContent = opis,
                    TeacherId = int.Parse(Session["Id"].ToString()),
                    SubjectId = grades.GetSubjectByName(this.przedmiot).SubjectId
                };

                grades.AddGrade(ocena);
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            WypelnijGV();            
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            GridView1.ShowFooter = false;
            WypelnijGV();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            WypelnijGV();    
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        { 
            for (int i = 0; i < GridView1.Columns.Count; i++)
            {
                DataControlFieldCell objCell = (DataControlFieldCell)GridView1.Rows[e.RowIndex].Cells[i];
                GridView1.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
            }

            Student student = grades.GetStudentByNameAndSurname(nazwiskoIImie.Split(' ')[1], nazwiskoIImie.Split(' ')[0]);
            Grade ocena = student.Grades.ToList()[e.RowIndex - 1];
            double x;
            DateTime dt;
            bool czy_double;
            bool czy_data;

            czy_double = this.IsGoodValue(e.NewValues[0].ToString(), out x);
            czy_data = DateTime.TryParse(e.NewValues[1].ToString(), out dt);

            if (czy_double && czy_data)
            {
                ocena.GradeValue = x;
                ocena.GradeDate = dt;
                if (e.NewValues[2] != null)
                    ocena.GradeContent = e.NewValues[2].ToString();
                else
                    ocena.GradeContent = null;

                grades.SaveChanges();
            }          

            GridView1.EditIndex = -1;
            GridView1.ShowFooter = true;
            Response.Redirect(Request.RawUrl);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            double value;
            bool czy_ocena = double.TryParse(Session["ocena"].ToString(), out value);
            Session.Remove("ocena");

            int x;            
            if (czy_ocena && (value >= 1 && value <= 6) && int.TryParse(value.ToString(), out x))
            {
                Grade ocena = new Grade
                {
                    StudentId = grades.GetStudentByNameAndSurname(this.nazwiskoIImie.Split(' ')[1], this.nazwiskoIImie.Split(' ')[0]).StudentId,
                    GradeValue = x,
                    GradeDate = DateTime.Today,
                    GradeType = GradeType.Final,
                    GradeContent = "OCENA KOŃCOWA",
                    TeacherId = int.Parse(Session["Id"].ToString()),
                    SubjectId = grades.GetSubjectByName(this.przedmiot).SubjectId
                };

                PokazKontrolki(false);
                grades.AddGrade(ocena);
                Response.Redirect(Request.RawUrl);
            }

        }

        private bool IsGoodValue(string value, out double x)
        {
            bool czy_double = double.TryParse(value, out x);
            if (!czy_double)
                return false;

            if ((x >= 1 && x <= 6) && (x * 2) % 1 == 0)
                return true;            

            return false;
        }
    }
}