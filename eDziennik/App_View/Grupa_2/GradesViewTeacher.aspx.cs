﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using eDziennik.App_SchoolDb;
using eDziennik.App_Code.Grupa_2;
using System.Data;

namespace eDziennik.App_View.Grupa_2
{
    public partial class GradesViewTeacher : System.Web.UI.Page
    {
        IGrades grades;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["RoleName"].ToString() == "teacher")
            {
                grades = new Grades(null, Convert.ToInt32(Session["Id"]));

                if (!IsPostBack)
                {
                    try
                    {
                        string przedmiot = Request.Cookies["przedmiot"].Value;
                        UsunCiasteczko("przedmiot");

                        string klasa = Request.Cookies["klasa"].Value;
                        UsunCiasteczko("klasa");

                        string uczen = Request.Cookies["uczen"].Value;
                        UsunCiasteczko("uczen");

                        WypelnijPrzedmiotami();
                        Ustaw(przedmiot, ddlPrzedmioty);

                        WypelnijKlasami();
                        Ustaw(klasa, ddlKlasy);

                        WypelnijUczniami();
                        Ustaw(uczen, ddlUczniowie);

                        Button1.Visible = true;
                    }
                    catch (Exception)
                    {
                        WypelnijPrzedmiotami();
                    }

                }
            }

            else
            {
                Response.Redirect("/App_Error_Page/DefaultError.html");
            }
        }

        private void Ustaw(string nazwa, DropDownList ddl)
        {
            for (int i = 0; i < ddl.Items.Count; i++)
            {
                if (ddl.Items[i].ToString() == nazwa)
                {
                    ddl.SelectedIndex = i;
                    break;
                }
            }
        }

        private void UsunCiasteczko(string p)
        {
            HttpCookie clearCookie = new HttpCookie(p, null);
            clearCookie.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(clearCookie);
        }

        private void WypelnijPrzedmiotami()
        {
            IList<Subject> przedmioty = grades.TeacherSubjects().ToList();

            ddlPrzedmioty.Items.Clear();
            ddlPrzedmioty.Items.Add("");
            foreach (Subject przedmiot in przedmioty)
            {
                ddlPrzedmioty.Items.Add(przedmiot.SubjectName);
            }
        }

        protected void ddlPrzedmioty_SelectedIndexChanged(object sender, EventArgs e)
        {
            WypelnijKlasami();
        }

        private void WypelnijKlasami()
        {
            string nazwa = ddlPrzedmioty.SelectedItem.ToString();
            if (!string.IsNullOrWhiteSpace(nazwa))
            {
                Subject slectedSubject = grades.GetSubjectByName(nazwa);
                IList<Class> klasy = grades.TeacherClassesForSubject(slectedSubject).ToList();

                ddlKlasy.Items.Clear();
                ddlKlasy.Items.Add("");
                foreach (Class klasa in klasy)
                {
                    ddlKlasy.Items.Add(klasa.ClassRank + " " + klasa.ClassSymbol);
                }
            }
            else
            {
                ddlKlasy.Items.Clear();                
                Button1.Visible = false;
            }
            ddlUczniowie.Items.Clear();
        }

        protected void ddlKlasy_SelectedIndexChanged(object sender, EventArgs e)
        {
            WypelnijUczniami();
        }

        private void WypelnijUczniami()
        {
            String[] rankAndSymbol = ddlKlasy.SelectedItem.ToString().Split(' ');

            if (rankAndSymbol.Length > 1)
            {
                IList<Student> studenci = grades.TeacherStudentsForClass(grades.GetClassBySymbolAndRank(rankAndSymbol[0], rankAndSymbol[1])).OrderBy(s => s.StudentLastName).ToList();

                ddlUczniowie.Items.Clear();
                ddlUczniowie.Items.Add("");
                foreach (Student student in studenci)
                {
                    ddlUczniowie.Items.Add(student.StudentLastName + " " + student.StudentFirstName);
                }
                ddlUczniowie.Items.Add("Wszyscy uczniowie");
            }
            else
            {
                ddlUczniowie.Items.Clear();
                Button1.Visible = false;
            }
        }

        protected void ddlUczniowie_SelectedIndexChanged(object sender, EventArgs e)
        {
            String[] surnameAndName = (sender as DropDownList).SelectedItem.ToString().Split(' ');

            if (surnameAndName.Length > 1)            
                Button1.Visible = true;
            else
                Button1.Visible = false;
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlUczniowie_SelectedIndexChanged(ddlUczniowie, e);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Cookies.Add(new HttpCookie("przedmiot", ddlPrzedmioty.SelectedItem.ToString()));
            Response.Cookies.Add(new HttpCookie("klasa", ddlKlasy.SelectedItem.ToString()));
            Response.Cookies.Add(new HttpCookie("uczen", ddlUczniowie.SelectedItem.ToString()));
            Response.Redirect("SelectedStudent.aspx");
        }
    }
}