﻿using eDziennik.App_Code.Grupa_2;
using eDziennik.App_SchoolDb;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eDziennik.App_View.Grupa_2
{
    public partial class GradesViewStudent : System.Web.UI.Page
    {
        IGrades grades;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["RoleName"].ToString() == "student")
            {
                grades = new Grades(Convert.ToInt32(Session["Id"]), null);

                if (!IsPostBack)
                {
                    WypelnijPrzedmiotami();
                }
            }
            else
            {
                Response.Redirect("/App_Error_Page/DefaultError.html");
            }
            
        }

        private void WypelnijPrzedmiotami()
        {
            IList<Subject> subjects = grades.StudentSubjects().ToList();

            ddlPrzedmioty.Items.Clear();
            ddlPrzedmioty.Items.Add("");
            foreach (Subject subject in subjects)
            {
                ddlPrzedmioty.Items.Add(subject.SubjectName);
            }
        }

        protected void ddlPrzedmioty_SelectedIndexChanged(object sender, EventArgs e)
        {
            string nazwa = ddlPrzedmioty.SelectedItem.ToString();

            if (!string.IsNullOrWhiteSpace(nazwa))
            {
                Subject subject = grades.GetSubjectByName(nazwa);

                IList<Grade> gradesForSubject = grades.StudentGradesFromSubject(subject).ToList();

                if (gradesForSubject.Count > 0)
                {
                    Label2.Visible = false;

                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("Ocena", typeof(string)));
                    dt.Columns.Add(new DataColumn("Data", typeof(string)));
                    dt.Columns.Add(new DataColumn("Opis", typeof(string)));

                    DataRow dr = dt.NewRow();

                    foreach (Grade grade in gradesForSubject)
                    {
                        dr = dt.NewRow();
                        dr[0] = grade.GradeValue.ToString();
                        dr[1] = grade.GradeDate.ToShortDateString();
                        dr[2] = grade.GradeContent;

                        dt.Rows.Add(dr);
                    }

                    GridView1.DataSource = dt;
                }
                else
                    Label2.Visible = true;
            }
            else
            {                
                GridView1.DataSource = null;
            }

            GridView1.DataBind();
        }
    }
}