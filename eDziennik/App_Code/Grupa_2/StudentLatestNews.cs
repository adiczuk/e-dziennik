﻿using eDziennik.App_SchoolDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eDziennik.App_Code.Grupa_2
{
    public class StudentLatestNews : IStudentLatestNews
    {
        private SchoolDb dbContext;
        private int StudentId;

        public StudentLatestNews(int StudentId)
        {
            this.StudentId = StudentId;
            dbContext = new SchoolDb();
        }


        public IEnumerable<Grade> StudentGrades()
        {
            var studentGrades = dbContext.Grades.Where(x => x.StudentId == StudentId);
            return studentGrades;
        }

        public IEnumerable<Document> StudnetCommendations()
        {
            var studentCommendations = dbContext.Documents.Where(x => x.StudentId == StudentId && x.DocumentType == DocumentType.Commendation);
            return studentCommendations;
        }

        public IEnumerable<Document> StudnetReprimands()
        {
            var studentCommendations = dbContext.Documents.Where(x => x.StudentId == StudentId && x.DocumentType == DocumentType.Reprimand);
            return studentCommendations;
        }

        public IEnumerable<Event> StudentNextFiveEvents()
        {
            var studentNextFiveEvents = StudentNextEvents().Take(5);
            return studentNextFiveEvents;
        }

        public IEnumerable<Event> StudentNextEvents()
        {
            var student = dbContext.Students.FirstOrDefault(x => x.StudentId == StudentId);
            var classStudent = dbContext.Classes.FirstOrDefault(x => x.ClassId == student.ClassId);


            //classStudent.Events.Add(new Event { EventData = DateTime.Now, EventContent = "jedziem w góry" });
            //classStudent.Events.Add(new Event { EventData = DateTime.Now, EventContent = "jedziem w góry elegancko" });
            //classStudent.Events.Add(new Event { EventData = DateTime.Now, EventContent = "jedziem w góry bajerancko" });
            //classStudent.Events.Add(new Event { EventData = DateTime.Now, EventContent = "jedziem w góry extra" });
            //classStudent.Events.Add(new Event { EventData = DateTime.Now, EventContent = "jedziem w gory fajne" });



            var StudentNextEvents = classStudent.Events.OrderBy(x => x.EventData);
            return StudentNextEvents;
        }


        public IEnumerable<string> StudentWall()
        {
            IEnumerable<string> toReturn = new List<string> { };

            toReturn = toReturn.Concat(StudentGrades().Select(x => x.GradeDate + " OCENA: " + x.GradeValue + " " + x.Subject.SubjectName + " " + x.Teacher.TeacherFirstName + " " + x.Teacher.TeacherLastName));
            toReturn = toReturn.Concat(StudnetCommendations().Select(x => x.DocumentDate + " POCHWAŁA: " + x.DocumentContent + " " + x.Teacher.TeacherFirstName + " " + x.Teacher.TeacherLastName));
            toReturn = toReturn.Concat(StudnetReprimands().Select(x => x.DocumentDate + " NAGANA: " + x.DocumentContent + " " + x.Teacher.TeacherFirstName + " " + x.Teacher.TeacherLastName));
            //toReturn = toReturn.Concat(StudentNextEvents().Select(x => x.EventData + " WYDARZENIE: " + x.EventContent));


            return toReturn;
        }
    }
}