﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eDziennik.App_Code.Grupa_2
{
    public interface IAuthentication
    {
        Guid GetPersonIdByLogin(string Login);
        IEnumerable<String> GetPersonIdAndRoleByUserId(Guid UserId);     
    }
}
