﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eDziennik.App_SchoolDb;

namespace eDziennik.App_Code.Grupa_2
{
    public class Grades : IGrades
    {
        private SchoolDb context;
        private int? StudentId;
        private int? TeacherId;

        public Grades(int? StudentId, int? TeacherId)
        {
            this.context = new SchoolDb();
            this.StudentId = StudentId;
            this.TeacherId = TeacherId;
        }
        
        public Subject GetSubjectByName(string Name)
        {
            Subject founded = context.Subjects.FirstOrDefault(s => s.SubjectName == Name);
            return founded;
        }

        public Class GetClassBySymbolAndRank(string Rank, string Symbol)
        {
            Class found = context.Classes.FirstOrDefault(c => c.ClassSymbol == Symbol && c.ClassRank == Rank);
            return found;
        }

        public Student GetStudentByNameAndSurname(string Name, string Surname)
        {
            Student found = context.Students.FirstOrDefault(s => s.StudentFirstName == Name && s.StudentLastName == Surname);
            return found;
        }

        public IEnumerable<Subject> TeacherSubjects()
        {
            var techerSubjects = context.Subjects.Where(s => s.Teachers.Contains(context.Teachers.FirstOrDefault(t => t.TeacherId == TeacherId)));
            return techerSubjects;
        }

        public IEnumerable<Class> TeacherClassesForSubject(Subject Subject)
        {
            var teacherClasses = context.Classes.Where(c => c.Subjects.Contains(context.Subjects.FirstOrDefault(s => s.SubjectId == Subject.SubjectId)) && c.Teachers.Contains(context.Teachers.FirstOrDefault(t => t.TeacherId == TeacherId)));
            return teacherClasses;
        }

        public IEnumerable<Student> TeacherStudentsForClass(Class Class)
        {
            var teacherStudents = context.Students.Where(st => st.Class.ClassId == Class.ClassId);
            return teacherStudents;
        }

        public IEnumerable<Grade> TeacherStudentsGradesForSubject(Student Student, Subject Subject)
        {
            var techerStudentGrades = context.Grades.Where(g => g.Student.StudentId == Student.StudentId && g.SubjectId == Subject.SubjectId);
            return techerStudentGrades;
        }

        public IEnumerable<Subject> StudentSubjects()
        {
            Class klasa = context.Students.FirstOrDefault(s => s.StudentId == StudentId).Class;
            var studentSubjects = context.Subjects.Where(s => s.Classes.Contains(context.Classes.FirstOrDefault(c => c.ClassId == klasa.ClassId)));
            return studentSubjects;
        }

        public IEnumerable<Grade> StudentGradesFromSubject(Subject Subject)
        {
            var studentGradesFromSubject = context.Grades.Where(g => g.SubjectId == Subject.SubjectId && g.StudentId == StudentId);
            return studentGradesFromSubject;
        }

        public IEnumerable<Grade> StudentsGradesFromSubject(Subject Subject)
        {
            var studentsGradesFromSubject = context.Grades.Where(g => g.SubjectId == Subject.SubjectId);
            return studentsGradesFromSubject;
        }


        public void AddGrade(Grade Grade)
        {
            context.Grades.Add(Grade);
            context.SaveChanges();
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}