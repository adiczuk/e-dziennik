﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eDziennik.App_SchoolDb;

namespace eDziennik.App_Code.Grupa_2
{
    public class Authentication : IAuthentication
    {
        private SchoolDb context;

        public Authentication()
        {
            context = new SchoolDb();
        }

        public Guid GetPersonIdByLogin(string Login)
        {
            return context.aspnet_Users.First(u => u.LoweredUserName == Login.ToLower()).UserId;
        }

        public IEnumerable<String> GetPersonIdAndRoleByUserId(Guid UserId)
        {
            IList<String> result = new List<String>();

            try
            {
                Student student = context.Students.First(s => s.UserId == UserId);
                result.Add(student.StudentId.ToString());
                result.Add("student");
                return result;
            }
            catch (Exception)
            { }
            try
            {
                Teacher teacher = context.Teachers.First(t => t.UserId == UserId);
                result.Add(teacher.TeacherId.ToString());
                result.Add("teacher");
                return result;
            }
            catch (Exception)
            { }

            Admin admin = context.Admins.First(a => a.UserId == UserId);
            result.Add(admin.AdminId.ToString());
            result.Add("admin");

            return result;
        }
    }
}