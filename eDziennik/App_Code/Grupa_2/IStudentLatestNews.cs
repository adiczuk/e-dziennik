﻿using System;
namespace eDziennik.App_Code.Grupa_2
{
    public interface IStudentLatestNews
    {
        System.Collections.Generic.IEnumerable<eDziennik.App_SchoolDb.Grade> StudentGrades();
        System.Collections.Generic.IEnumerable<eDziennik.App_SchoolDb.Document> StudnetCommendations();
        System.Collections.Generic.IEnumerable<eDziennik.App_SchoolDb.Document> StudnetReprimands();

        System.Collections.Generic.IEnumerable<eDziennik.App_SchoolDb.Event> StudentNextFiveEvents();

        System.Collections.Generic.IEnumerable<eDziennik.App_SchoolDb.Event> StudentNextEvents();
        System.Collections.Generic.IEnumerable<string> StudentWall();
    }
}
