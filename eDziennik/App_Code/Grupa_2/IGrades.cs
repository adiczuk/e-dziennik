﻿using System;
using System.Collections.Generic;
using eDziennik.App_SchoolDb;

namespace eDziennik.App_Code.Grupa_2
{
    public interface IGrades
    {
        Subject GetSubjectByName(string Name);
        Class GetClassBySymbolAndRank(string Rank, string Symbol);
        Student GetStudentByNameAndSurname(string Name, string Surname);

        IEnumerable<Subject> StudentSubjects();
        IEnumerable<Grade> StudentGradesFromSubject(Subject Subject);
        IEnumerable<Grade> StudentsGradesFromSubject(Subject Subject);

        IEnumerable<Subject> TeacherSubjects();
        IEnumerable<Class> TeacherClassesForSubject(Subject Subject);
        IEnumerable<Student> TeacherStudentsForClass(Class Class);

        IEnumerable<Grade> TeacherStudentsGradesForSubject(Student Studnet, Subject Subject);

        void AddGrade(Grade Grade);
        void SaveChanges();
    }
}
